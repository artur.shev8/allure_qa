import json
import logging
import os

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())
ENV = os.getenv("ENV")
logging.info(f"Environment is: {ENV}")

CONFIGS_DIR_PATH = os.path.dirname(__file__)
config_file_name = os.path.join(CONFIGS_DIR_PATH, "envs", f"{ENV}.json")
with open(config_file_name, "r") as config_file:
    env_config = config_file.read()

env_config = json.loads(env_config)
logging.info(f"CONFIGS IS:\n{env_config}")

BASE_URL = env_config["base_url"]
