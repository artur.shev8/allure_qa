import logging

from src.configs.property_loader import BASE_URL


class FunctionHelper:

    @staticmethod
    def add_two_numbers(a: float, b: float):
        logging.info(f"Test on {BASE_URL} environment")
        return a + b

    @staticmethod
    def multiply_two_numbers(a: float, b: float):
        logging.info(f"Test on {BASE_URL} environment")
        return a * b
