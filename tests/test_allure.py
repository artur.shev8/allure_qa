import allure
import pytest


def test_1():
    assert 1 == 1


@allure.step
def fill_username(param1):
    pass


@allure.step
def fill_password(param1):
    pass


@allure.step
def click_login_button(elem):
    pass


@allure.epic("Users")
@allure.feature("login")
@allure.title("happy path for login user")
@allure.description("this test verifies that user can log in")
@allure.severity(allure.severity_level.BLOCKER)
def test_login():
    fill_username("qwe")
    fill_password("asd")
    click_login_button("elem1")


@allure.epic("Attachments")
@allure.feature("links")
@allure.link("https://yalantis.com/", name='Some IT company')
@allure.testcase("https://yalantis.com/", 'TestCase123')
@allure.issue("https://yalantis.com/", 'Issue123')
def test_logout():
    fill_username("qwe")
    fill_password("asd")
    click_login_button("elem1")


@allure.epic("Attachments")
@allure.feature("files")
def test_attaches():
    allure.attach.file('tests/img.png', attachment_type=allure.attachment_type.PNG)
    allure.attach('<head></head><body> Hello from HTML</body>', 'Allure attach html file', allure.attachment_type.HTML)
    allure.attach('A text file for report', 'blah bla', allure.attachment_type.TEXT)


@allure.epic("Parametrized tests")
@allure.feature("Parametrized tests")
@pytest.mark.parametrize('param1', [True, False], ids=['some P1 val1', 'some P1 val2'])
@pytest.mark.parametrize('param2', ['val1', 'val2'], ids=['some P2 val1', 'some P2 val2'])
def test_with_two_parameter(param1, param2):
    fill_username(param1)
    fill_password(param2)


@allure.epic("Parametrized tests")
@allure.feature("Parametrized tests")
@pytest.mark.parametrize('param1', [True, False])
@pytest.mark.parametrize('param2', ['val1', 'val2'])
def test_with_two_parameter_without_ids(param1, param2):
    fill_username(param1)
    fill_password(param2)
