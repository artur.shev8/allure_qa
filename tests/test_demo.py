import pytest

from src.services.functions import FunctionHelper


def test_1():
    assert FunctionHelper.add_two_numbers(1, 2) == 3, 'Error in adding two values'


def test_2():
    assert False, 'Assertion incorrect!'


def test_3():
    assert 1 == 1, 'Not equal'


def test_4():
    assert 1 == 2, 'Not equal'


def test_6():
    assert 1 == 1, 'Not equal'


def test_7():
    assert 1 == 1, 'Not equal'


def test_8():
    assert 1 == 3, 'Not equal'


def test_9():
    assert 1 == 3, 'Not equal'


def test_10():
    assert 1 == 3, 'Not equal'


def test_11():
    raise Exception("some test exception")


def test_12():
    1 / 0


def test_13():
    assert 1 == 1


def test_14():
    assert 2 == 2


@pytest.mark.skip(reason='skipped due to some reason')
def test_skippped():
    pass


@pytest.mark.xfail(reason='failed dut to poor knowledge in math')
def test_failed():
    assert 1 == 2
